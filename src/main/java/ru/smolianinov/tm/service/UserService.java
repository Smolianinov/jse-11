package ru.smolianinov.tm.service;


import ru.smolianinov.tm.entity.Task;
import ru.smolianinov.tm.entity.User;
import ru.smolianinov.tm.repository.UserRepository;

import java.util.List;

public class UserService {

    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    /**
     * Искать пользователя по логину
     *
     * @param login логин
     * @return
     */
    public User findByLogin(String login) {
        if (login == null || login.isEmpty()) return null;
        return userRepository.findByLogin(login);
    }

    /**
     * Создать пользователя
     *
     * @param login    логин
     * @param password логин
     * @param fio      ФИО
     * @return
     */
    public User create(final String login, final String password, final String fio) {
        if (login.isEmpty() || password.isEmpty()) {
            System.out.println("LOGIN/PASSWORD CAN'T BE EMPTY");
            return null;
        }
        //if (description == null || description.isEmpty()) return null;
        return userRepository.create(login, password, fio);
    }

    public User removeByLogin(String login) {
        if (login == null || login.isEmpty()) return null;
        return userRepository.removeByLogin(login);
    }

    /**
     * Изменить ФИО
     *
     * @param login логин
     * @param fio   ФИО
     * @return
     */
    public User updateFio(String login, String fio) {
        if (login.isEmpty() || fio.isEmpty()) return null;
        return userRepository.updateFio(login, fio);
    }

    /**
     * Изменить Пароль
     *
     * @param login    логин
     * @param password ФИО
     * @return
     */
    public User updatePassword(String login, String password) {
        if (login.isEmpty() || password.isEmpty()) return null;
        return userRepository.updatePassword(login, password);
    }


    public void clear() {
        userRepository.clear();
    }

    public List<User> findAll() {
        return userRepository.findAll();

    }

}
