package ru.smolianinov.tm.controller;

import ru.smolianinov.tm.entity.Project;
import ru.smolianinov.tm.entity.Task;
import ru.smolianinov.tm.entity.User;
import ru.smolianinov.tm.repository.UserRepository;
import ru.smolianinov.tm.service.UserService;
import ru.smolianinov.tm.utils.Md5;


public class UserController extends AbstractController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    public int createUser() {
        System.out.println("[CREATE USER]");
        System.out.println("PLEASE, ENTER LOGIN:");
        final String login = scanner.nextLine();
        if (login.isEmpty()) {
            System.out.println("[FAIL - LOGIN DON'T BE EMPTY]");
            return 0;
        }
        System.out.println("PLEASE, ENTER FIO:");
        final String fio = scanner.nextLine();
        if (fio.isEmpty()) {
            System.out.println("[FAIL - FIO DON'T BE EMPTY]");
            return 0;
        }
        System.out.println("[PLEASE, ENTER NEW PASSWORD]");
        final String newPasswordFirst = scanner.nextLine();
        System.out.println("[PLEASE, ENTER NEW PASSWORD AGAIN]");
        final String newPasswordSecond = scanner.nextLine();
        if (!newPasswordFirst.equals(newPasswordSecond)) {
            System.out.println("[FAIL - PASSWORD DON'T MATCH]");
            return 0;
        }
        userService.create(login, newPasswordFirst, fio);
        System.out.println("[OK]");
        return 0;
    }

    public int clearUser() {
        System.out.println("[CLEAR USER]");
        userService.clear();
        System.out.println("[OK]");
        return 0;
    }

    public void viewUser(final User user) {
        if (user == null) return;
        System.out.println("[VIEW USER]");
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("FIO: " + user.getFio());
        System.out.println("[OK]");
    }

    public int viewUserByLogin() {
        System.out.println("ENTER, LOGIN:");
        final String login = scanner.nextLine();
        final User user = userService.findByLogin(login);
        viewUser(user);
        return 0;
    }

    public int removeUserByLogin() {
        System.out.println("[REMOVE USER BY LOGIN]");
        System.out.println("PLEASE, ENTER LOGIN:");
        final String login = scanner.nextLine();
        final User user = userService.findByLogin(login);
        if (user == null) System.out.println("[FAIL]");
        System.out.println("PLEASE, ENTER PASSWORD:");
        final String password = Md5.md5Apache(scanner.nextLine());
        final String passwordEtalon = user.getPassword();
        if (!password.equals(passwordEtalon)) {
            System.out.println("[FAIL PASSWORD]");
            return 0;
        } else {
            System.out.println("[OK]");
            final User user1 = userService.removeByLogin(login);
        }
        return 0;
    }

    public int listUser() {
        System.out.println("[LIST USER]");
        int index = 1;
        for (final User user : userService.findAll()) {
            System.out.println(index + ". " + user.getLogin() + " | " + user.getPassword() + " | " + user.getFio());
            index++;
        }
        System.out.println("[OK]");
        return 0;
    }

    public int updateUserFioByLogin() {
        System.out.println("[UPDATE USER]");
        System.out.println("ENTER LOGIN:");
        final String login = scanner.nextLine();
        final User user = userService.findByLogin(login);
        if (user == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("[PLEASE, ENTER FIO]");
        final String fio = scanner.nextLine();
        userService.updateFio(login, fio);
        System.out.println("[OK]");
        return 0;
    }

    public int updateUserPasswordByLogin() {
        System.out.println("[UPDATE USER]");
        System.out.println("ENTER LOGIN:");
        final String login = scanner.nextLine();
        final User user = userService.findByLogin(login);
        if (user == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("[PLEASE, ENTER OLD PASSWORD]");
        final String oldPassword = Md5.md5Apache(scanner.nextLine());
        final String oldPasswordEtalon = user.getPassword();
        if (!oldPasswordEtalon.equals(oldPassword)) {
            System.out.println("[FAIL PASSWORD]");
            return 0;
        }
        System.out.println("[PLEASE, ENTER NEW PASSWORD]");
        final String newPasswordFirst = scanner.nextLine();
        System.out.println("[PLEASE, ENTER NEW PASSWORD AGAIN]");
        final String newPasswordSecond = scanner.nextLine();
        if (!newPasswordFirst.equals(newPasswordSecond)) {
            System.out.println("[FAIL - PASSWORD DON'T MATCH]");
            return 0;
        }
        userService.updatePassword(login, Md5.md5Apache(newPasswordFirst));
        System.out.println("[OK]");
        return 0;
    }

}